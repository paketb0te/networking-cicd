# Kosten - Nutzen Abwägung

## Kosten / Nachteile / Challenges

### Finanzielle Aufwendungen

- Es müssen ein paar Server zusätzlich betrieben werden
  - git
  - cicd (entweder kombiniert in gitlab, oder separat, zB Jenkins, drone.io, ...)
  - oxidized
  - ansible / Orchestrierungstool
  - Virtualisierungsserver (für Netzwerkdevices)
- Initiale Integration
  - Aufsetzen der Virtualisierungsserver
  - Anpassung an die bestehende Infra evtl langwierig
  - Schulung der Mitarbeiter
  - Schreiben von initialen Tests

### Kulturelle Herausforderungen

- Die einführung von DevOps-Prinzipien im Netzwerkbereich erfordert, wie auch im Serverbereich, einen Wandel der Unternehmenskultur / Herangehensweise.
- Möglicherweise Widerstand gegen Änderungen von Seiten der Mitarbeiter
- Echtes Buy-In vom Management und Team nötig.

## Vorteile

### Im laufenden Betrieb

#### nur configmanagement in git (oxidized)

- Klare Nachvollziehbarkeit von Konfigurationen und deren Änderungen im Zeitverlauf (wer hat was wann warum geändert)
- Daraus resultierend: Erhöhte "Ownership" von Changes.
- Einfacher Rollback (git revert) zu vorherigen Zuständen möglich

#### mit CI/CD

- automatisierte Tests der geplanten Änderungen möglich
  - Tests können beliebig einfach / komplex definiert werden.
    - reines Linting (Syntaxüberprüfung)
    - rückgabe des diffs zwischen aktueller config und config nach dem change
    - Ausführung von Tests in einer virtuellen Umgebung
    - Ausführung von Tests gegen dedizierte Hardware
- bei bestehen aller relevanten Tests (Teil-) automatisierter Rollout der Änderungen möglich
  - entweder mit manueller Freigabe durch Berechtigte Person
  - alternativ vollautomatisert
