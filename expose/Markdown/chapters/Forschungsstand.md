# Forschungsstand

NetDevOps / Network Automation werden bisher hauptsächlich in großen / komplexen Umgebungen betrachtet und angewendet (vgl NetDevOps Survey 2019: 87% der Befragten betreiben Netzwerke mit 50+ Devices, 67% sogar mit 250+ Devices, wobei natürlich alle dort Befragten bereits mit NetDevOps vertraut sind oder (Teile davon) anwenden - die Anzahl der kleinen, nicht-automatisierten Netzwerke dürfte um ein vielfaches höher sein).

Dieses Resultat ist nicht überraschend, da der (technische) Implementationsaufwand nur unwesentlich von der Netzwerkgröße abhängt (also ähnlich hoch für kleine und große Netze sein dürfte), während die Auswirkungen und Vorteile tendenziell linear mit der Anzahl der zu verwaltenden Geräte steigen.

Der Ansatz, Netzwerke zu virtualisieren und Tests gegen diese Klone auszuführen, ist nicht neu, und kann z.B. im Software-defined Networking mit OpenFlow / ONOS bereits eingesetzt werden (Continuous Integration of Applications for ONOS, 2019), da hier das zu verifizierende Netzwerk bereits größtenteils von der darunterliegenden Hardware abstrahiert wurde.

Auch im Forschungsbereich gibt es diverse Tools zur Simulation / Emulation von Netzwerken (z.B. Mininet, NEmu), welche jedoch typischerweise nicht konkrete Software-Images von bestimmten Herstellern abbilden, sondern Router als Instanz einer minimalen Linux-Distribution virtualisieren.

In Netzwerken mit "klassischer" Hardware besteht das Problem, dass die Hardware nie exakt emuliert werden kann - das reicht von kosmetischen Unterschieden wie z.B. unterschiedliche Interface-Bezeichnungen bis hin zu grundlegenden Unterschieden in der Funktionalität (z.B. existiert keine Virtualisierungslösung, welche den Datendurchsatz von ASICS abbilden kann, was Performancetests für virtuelle Switches quasi unmöglich macht).

Zu klären bleibt die Frage, inwiefern sinnvolle / aussagekräftige Tests gegen virtuelle Klone eingesetzt werden können, TROTZ der Unterschiede zwischen Original und Klon.
