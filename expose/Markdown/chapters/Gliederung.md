# Gliederung

## Einleitung

### Infrastructure as Code

### DevOps

### NetDevOps & Network as Code

## Grundlagen / Stand der Forschung

### Überblick DevOps

#### Kulturelle / Operationale Aspekte

- Agile Methoden
- Überwindung von Silos: Dev + Ops
- Incident Management / Fehlerkultur / Post Mortems

#### Technische Aspekte / Toolchains

- VCS (git)
- Continuous Integration & Delivery Toolchains
- Unterschiedliche Umgebungen
  - Entwicklung
  - Test
  - Abnahme
  - Produktion
- Change Management / Release Automation
- Monitoring

## Konzept

### Analyse von DevOps-Prozessen auf Anwendbarkeit im Netzwerkbereich

### Vergleich und Auswahl passender Tools

#### VCS

- gitlab
- gitea
- github
  
#### Continuous Integration & Delivery

- Ansible
- Terraform
- Chef
- Puppet
- SaltStack
- Jenkins
- drone.io

#### Virtuelle Umgebungen für Testing

- EVE-NG
- GNS3
- VIRL
- vrnetlab (docker)

## Implementierung

### Implementation der im vorherigen Abschnitt ausgewählten Prozesse und Tools - die konkrete Implementation ergibt sich im Anschluss an die Analyse

## Auswertung

### Operationale Aspekte

#### Neue Prozesse

#### Akzeptanz

### Technische Aspekte

#### Toolauswahl

#### Zusammenspiel der Tools

## Fazit

### Rückblick

### Zusammenfassung der Auswertung

### Mögliche nächste Schritte
