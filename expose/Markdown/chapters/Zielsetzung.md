# Zielsetzung

Für die Beantwortung der Forschungsfrage sollen zunächst unterschiedliche DevOps-Prozesse samt dazugehöriger Tools analysiert und verglichen werden.

Nachdem eine Auswahl passender Prozesse und Tools gefunden wurde, sollen diese stufenweise im Netzwerkmanagement eines mittelständischen IT-Dienstleisters (system.de) implementiert werden.

Abschließend soll der durch den NetDevOps-Ansatz generierte Mehrwert betrachtet und in Relation zum Implementierungsaufwand gesetzt werden.
