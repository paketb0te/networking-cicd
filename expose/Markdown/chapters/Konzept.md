# Konzept

## Analyse von DevOps-Prozessen auf Anwendbarkeit im Netzwerkbereich

Innerhalb des Network-as-Code-Paradigmas wird zwar, wie der Name bereits erahnen lässt, die gesamte Netzwerkkonfiguration als Code betrachtet, doch gibt es natürlich trotzdem Unterschiede in den Anforderungen an die Entwicklung dieses Codes.

Häufig genannte Vorteile von DevOps sind z.B.:

- schnellere Time-to-Market / kürzere Release-Zyklen
- höhere Code-Qualität
- transparenteres Arbeiten & bessere Kommunikation innerhalb und zwischen Teams
- kürzere Problemlösungszeiten / Rollback
- bessere Skalierbarkeit
- Verbessertes Feedback, kürzere Feedback-Schleifen

Diese Vorteile sind für den Netzwerkbetrieb von unterschiedlicher Bedeutung - erhöhte Stabilität und kürzere Problemlösungszeiten sind definitiv sehr erstrebenswert, wohingegen Time-to-Market und die Geschwindigkeit von Release- (bzw. Change-) Zyklen eher weniger wichtig erscheinen.

Zunächst werden daher einige der gängigsten DevOps-Prozesse auf ihre Relevanz und Anwendbarkeit für den Netzwerkbetrieb hin untersucht.

Anschließend werden die dazu passenden Tools / Toolchains analysiert, verglichen und ausgewählt.

Ein wichtiger Teil von CI/CD sind automatisierte Tests.
Hier gibt es einige Unterschiede zu DevOps, da in den meisten nichttrivialen Fällen Funktionen im gesamten Netzwerk getestet / verifiziert werden müssen.
Im Idealfall existiert ein separates Netzwerk mit identischer Topologie und Hardware, gegen welches Tests gefahren werden können - in der Praxis dürfte dieser Fall jedoch so gut wie nie eintreten.
Alternativ könnten virtuelle Umgebungen verwendet werden, die idealerweise einen möglichst exakten Klon der realen Netzwerkhardware und -topologie erzeugen - die CI/CD Pipeline führt dann die definierten Tests gegen diesen Klon anstatt gegen das produktive Netzwerk aus. Der springende Punkt ist hier natürlich das Wort "exakt", es dürfte nahezu unmöglich sein einen identischen virtuellen Klon zu erzeugen, allerdings sollte es durchaus möglich sein, sinnvolle Tests auch gegen einen "ähnlich genugen" Klon auszuführen.
Aus diesem Grund werden die wichtigsten Virtualisierungslösungen für Netzwerke analysiert und ihre Features gegenübergestellt, um einen Kandidaten für ein Proof of Concept auszuwählen.

Nachdem die zu verwendenden Tools ausgewählt wurden, soll die technische Umsetzung erfolgen, wobei hier (angelehnt an den Vortrag von Leslie Carr auf der SREcon16 und Talk von Patrick Moore auf der NANOG72) folgende Reihenfolge einzelner kleiner in sich abgeschlossener Schritte angedacht ist:

1. Übernahme der Konfigurationsdateien in ein Versionskontrollsystem (git)
2. Umwandeln von Konfigurationen in Templates
3. Aufsetzen eines CI/CD Tools
4. Einfache Tests als Proof of Concept für CI/CD-Pipelines schreiben
5. (semi-) automatisches Deployment von Konfigurationsänderungen
6. VIrtualisierungslösung aufsetzen
7. Tests gegen virtuelles Netzwerk ausführen
