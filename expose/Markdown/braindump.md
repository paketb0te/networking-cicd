# Braindump

es dürfte vorteile bringen, im bereich des netzwerkmanagements und -betriebs die selben oder ähnliche Prinzipien anzuwenden, wie sie seit einigen Jahren im Zuge der Verbreitung der "DevOps-Bewegung" in der Softwareentwicklung und im Betrieb von Serverinfrastrukturen Anwendung finden.

DevOps bringt den tendenziell eher "starren" (im Sinne von: zuverlässig, stabil) Bereich des Infrastrukturbetriebs mit den seit einiger Zeit zunehmend flexibleren, agilen Methoden der Softwareentwicklung zusammen.

Ein häufig im DevOps-Umfeld anzutreffendes Element der Anwendungsintegration sind sogenannte Continuous Integration / Continuous Delivery Pipelines (CI/CD-Pipelines).

Sinn und Zweck solcher Pipelines ist die automatische Verifizierung neuen oder veränderten Codes durch Tests.

## Ein typisches Szenario könnte bspw. wie folgt aussehen

Ein Entwickler erweitert einen Teil der Anwendung um ein bestimmtes Feature und checkt seinen Code in den Entwicklungszweig des zentralen git-repositorys ein.
Dadurch wird eine "Pipeline" gestartet, welche z.B. statische Code-Analysen durchführt (ist der Code syntaktisch korrekt?), einen bestimmten Satz an Unittests aufruft (sind die einzelnen Bestandteile / Einheiten des Codes in Ordnung?), und wenn alle Tests erfolgreich waren, einen Pull-Request für den Master-Branch erzeugt, der dann noch final von einer autorisierten Person genehmigt wird. (Der letzte Teil könnte natürlich auch automatisiert passieren, sofern man ausreichend Tests hat...)

## Analog dazu könnte der Workflow im Netzwerkbereich so aussehen

Ein Netzwerkadmin möchte eine Konfigurationsänderung an einem Switch durchführen. Er lädt die aktuelle Konfiguration des betreffenden Switches per `git pull` aus dem zentralen Konfigurations-Repository, ändert die entsprechenden Zeilen, und schreibt die Änderungen via `git commit & git push` zurück ins Repo (ggfs. auch in einen separaten Branch).
Dadurch wird eine Pipeline angestoßen, welche auch hier eine Testsuite ausführt (Denkbar wäre z.b. ganz simpel: Syntax-Prüfung. Erreichbarkeit aller Geräte überprüfen. Ooptimalerweise wurde vor dem commit ein Test geschrieben, der das erwartete Verhalten verifiziert.).
Sind alle Tests erfolgreich, darf der Admin die Änderung über einen Pull-Request in den Master-Branch übernehmen und die Konfigurationsänderung wird automatisch auf den Switch ausgerollt (zB. via Ansible, SaltStack, ...).

### IaC

Im Serverbereich existiert seid einiger Zeit das Konzept von "Infrastructure as Code", gemeint ist damit die "Verwaltung und Provision von Rechenzentren durch maschinenlesbare Konfigurationsdateien anstatt physischer Hardwarekonfiguration oder interaktiver Konfigurationstools." [Wikipedia](https://en.wikipedia.org/wiki/Infrastructure_as_code)

Dieses Konzept lässt sich natürlich auch auf Netzwerkhardware erweitern und bringt auch dort Vorteile:

- reduzierte Kosten (nach initialer Einrichtung weniger menschliche Arbeitszeit erforderlich)
- Geschwindigkeit (minimale manuelle Konfiguration erforderlich, typischerweise automatisierte provision via templates)
- Verminderte Risiken (menschliche Fehler werden reduziert - andererseits könnte natürlich ein Fehler zB in einem Template deutlich weitreichendere Schäden anrichten - hier gilt es, diese Abzuwägen)

In großen Rechenzentren existieren bereits heute CI/CD Pipelines für Netzwerkinfrastruktur, im Enterprise-Bereich dürfte dieses Thema noch nicht so weit verbreitet sein.

Es wäre praktisch, wenn auch mittelgroße Unternehmen die Vorzüge solcher CI/CD Pipelines benutzen könnten - Probleme könnten hier die heterogenere Hardwarelandschaft (?) sein, sowie ein Rechtfertigungsdruck für zusätzliche Ausgaben für Hardware, die nicht produktiv eingesetzt, sondern nur für Tests eingesetzt wird.
(Daher auch der Satz: "Jede Firma hat ein Test-Netzwerk. Manche Firmen haben sogar ein separates Produktions-Netzwerk!").

Wenn eine CI/CD Pipeline Tests gegen eine virtuelle Instanz des Produktiven Netzwerks ausführen könnte, dürfte das deutliche Einsparungen bei (hoffentlich) geringem Verlust der Aussagekraft der Tests bringen.
