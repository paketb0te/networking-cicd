# Expose

## Deckblatt

## Forschungsthema

## Zielsetzung

## Forschungsstand / Theoretische Grundlage

## Konzept

## Gliederung

Die einzelnen Stufen

- config -> git (oxidized)
- syslog-hook schreibt Änderungen automatisch ins git
- Config-Templates
- git hook schreibt bei commit änderung auf devices
- einführung von tests, CI/CD Pipelines
  - nur diff der geplanten Änderung
  - syntax / typos
  - funktional
    - virtuelle Umgebung
      - vergleich unterschiedlicher Virtualisierungsplattformen
        - EVE-NG
        - GNS3
        - VIRL
        - ...
    - dedizierte Hardware
  - "test driven development"
- r

## Zeitplan

## (Motivation)

## Literaturverzeichnis

## bla

Continuous Integration and Delivery in the Enterprise Network.

Developing a DevOps Workflow for the Enterprise Network.

DevOps-Tools im Betrieb kleinerer Rechnernetze

Developing an evaluation framework for network automation
