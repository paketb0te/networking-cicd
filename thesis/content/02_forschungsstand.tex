\chapter{Forschungsstand}
\label{cha:forschungsstand}

\section{DevOps}

\forceindent Der Begriff \textit{DevOps} ist in der wissenschaftlichen Literatur nicht einheitlich definiert: \glqq There is no consensus of what concepts DevOps covers, nor how DevOps is defined\grqq{} \cite[4]{Erich.2017}. Es gibt sogar Metastudien, die sich ausschließlich mit der Klärung des Begriffes DevOps beschäftigen \cite{Jabbari.2016}.

Aus diesem Grund wird zunächst eine Definition des Begriffes vorgenommen, wie er im weiteren Verlauf dieser Arbeit zu verstehen ist. Hierbei wird bewusst der Fokus auf die für den Netzwerkbetrieb relevanteren Bereiche gelegt.

DevOps ist ein Kunstwort aus den Begriffen  \textit{Development} ((Software-) Entwicklung) und \textit{Operations} (IT-Betrieb) \cite{Jabbari.2016} und beschreibt einen Ansatz, welcher eine engere Zusammenarbeit zwischen den beiden (klassischerweise in unterschiedlichen Abteilungen angesiedelten) Teams ermöglichen und fördern soll \cite{Schapiro.2019}. Dieser Ansatz umfasst nach gängiger Auffassung drei Bereiche (auch als die \textit{DevOps Trinity} bezeichnet): \textbf{Menschen, Prozesse \& Tools} \cite{Farcic.2018}.

Dies bedeutet, dass DevOps nicht einfach eine Ansammmlung bestimmter Prozesse und Tools ist, sondern viel mehr als eine Arbeitsphilosophie zu verstehen ist. Gegenseitiges Aufgabenverständnis und gute Zusammenarbeit ermöglichen den verschiedenen Stakeholdern eines Softwareprojektes, ein besseres Produkt abzuliefern (wobei \glqq Produkt\grqq{} in diesem Zusammenhang nicht lediglich der Code ist, sondern das beim Endnutzer sichtbare Ergebnis. Dieses besteht aus qualitativ hochwertigem Code, hoher Verfügbarkeit bzw. Stabilität und kontinuierlicher Verbesserung).

Die konkreten Prozesse und Tools ergeben sich aus den Anforderungen, welche die Entwickler und Betreiber zur Erfüllung dieses Zieles definieren. Trotz der grundsätzlich freien Wahl der Prozesse gibt es einige, die sich in vielen Umgebungen bewährt haben und daher oft als Teil von bzw. synonym zu DevOps betrachtet werden \cite{Jabbari.2016}:

\begin{itemize}
    \item Agile Entwicklungsmethoden (\glqq DevOps extends Agile\grqq{})
    \item Hoher Automationsgrad
    \item Continuous Integration
    \item Continuous Delivery
    \item Continuous Deployment
\end{itemize}

Diese Prozesse und die dazugehörigen Tools bzw. Toolchains werden im Weiteren auf ihre Relevanz und Anwendbarkeit für den Betrieb kleiner Rechnernetze untersucht.

\section{NetDevOps}
\label{sec:forschungsstand:netdevops}
\forceindent NetDevOps und Network Automation werden bisher hauptsächlich in großen, komplexen Umgebungen betrachtet und angewendet (vgl. NetDevOps Survey 2019 \cite{DamienGarros.2019}: 87\% der Befragten betreiben Netzwerke mit 50+ Devices, 67\% sogar mit 250+ Devices, wobei alle dort Befragten bereits mit NetDevOps vertraut sind oder zumindest in Teilen anwenden -- die Anzahl der kleinen, nicht-automatisierten Netzwerke dürfte um ein vielfaches höher sein).

Dieses Resultat ist nicht überraschend, da der Implementationsaufwand nur unwesentlich von der Netzwerkgröße abhängt (also ähnlich hoch für kleine und große Netze sein dürfte), während die Auswirkungen und Vorteile tendenziell linear mit der Anzahl der zu verwaltenden Geräte steigen.

Der Ansatz, Netzwerke zu virtualisieren und Tests gegen diese virtuellen Klone auszuführen, ist nicht neu, und kann \zB im Software-defined Networking mit ONOS und OpenFlow bereits eingesetzt werden \cite{M.Gromann.2019}, da hier das zu verifizierende Netzwerk bereits größtenteils von der darunterliegenden Hardware abstrahiert wurde.

\begin{description}
    \item[Software-defined Networking] -- kurz \acs{sdn} -- bedeutet im einfachsten Fall eine Entkopplung von \textit{Kontrollebene} und \textit{Datenebene} \cite{Foundation.2012}. In klassischer Netzwerk-Hardware sind diese beiden Elemente in den einzelnen Geräten vereint. Das jeweilige Betriebssystem bildet die Kontrollebene und steuert, \zB mit Routing-Protokollen, wie genau die \acs{asics} Daten weiterleiten. Mit \acs{sdn} kann die Kontrollebene zentralisiert ausgeführt werden und dadurch ggfs. intelligentere Entscheidungen treffen. Eine solche zentralisierte Kontrollebene wird als \textit{SDN-Controller} bezeichnet.
    \item[ONOS] steht für Open Network Operating System und ist ein solcher SDN-Controller, es wird also nicht auf den Netwerkgeräten direkt ausgeführt, sondern auf einem oder mehreren zentralen Servern. Es stellt die Kontrollebene für das gesamte Netzwerk dar.
    \item[OpenFlow] ist ein Protokoll, das auf Switches ausgeführt wird und eine Schnittstelle für SDN-Controller zur Verfügung stellt, über welche die Forwarding-Logik des Switches gesteuert werden kann.
\end{description}

Auch im Forschungsbereich gibt es diverse Tools zur Simulation und Emulation von Netzwerken (\zB Mininet \cite{Lantz.2010}, NEmu \cite{Autefage.2016}), welche jedoch typischerweise nicht konkrete Software-Images bestimmter Hersteller abbilden, sondern Router als Instanz einer minimalen Linux-Distribution virtualisieren.

\begin{description}
    \item[Mininet] ist ein Netzwerk-Emulator und kann Netzwerke virtueller Hosts, Switches, Controller und Links erzeugen. Mininet verwendet prozess-basierte Virtualisierung (\textit{containerization}) und macht sich die Funktion von \textit{network namespaces} des Linux-Kernels zunutze \cite{MininetTeam.28.08.2018}. Diese Architektur macht Mininet sehr leistungsfähig, ist aber auch der Grund, warum es nicht für den Einsatz im NetDevOps-Umfeld geeignet ist -- es können keine \glqq{}echten\grqq{} Images der Netzwerk-Betriebssysteme verwendet werden.
    \item[NEmu] ist auch ein Netzwerk-Emulator, verwendet aber im Gegensatz zu Mininet keine Virtualisierung auf Prozessebene, sondern \acs{qemu} \cite{Autefage.2016}. Dadurch werden ganze Systeme emuliert, was die Architektur nicht ganz so leichtgewichtig macht wie Mininet. Ein großer Vorteil von NEmu ist die Möglichkeit, die Virtualisierung auf mehrere Server zu verteilen, wodurch es skalierbar wird. Der integrierte virtuelle Router kann zwar durch Plugins funktionell erweitert werden, trotzdem besteht hier im Bezug auf NetDevops das gleiche Problem wie bei Mininet, da nur der interne virtuelle Router verwendet werden kann.
\end{description}

In Netzwerken mit \glqq klassischer\grqq{} Hardware besteht das Problem, dass die Hardware nie exakt emuliert werden kann -- das reicht von kosmetischen Unterschieden wie beispielsweise unterschiedlichen Interface-Bezeichnungen bis hin zu grundlegenden Unterschieden in der Funktionalität (\zB können Virtualisierungslösungen per Definition keine aussagekräftigen Performancetests durchführen, da sie eben nicht auf den entsprechenden \acs{asics} ausgeführt werden).

Zu klären bleibt die Frage, inwiefern sinnvolle und aussagekräftige Tests gegen virtuelle Klone möglich sind, trotz der Unterschiede zwischen Original und Klon.
