\chapter{Auswertung}
\label{cha:auswertung}
\forceindent In diesem Kapitel werden die Erkenntnisse aus der Analyse von DevOps-Prozessen und -Tools sowie die in der technischen Umsetzung gemachten Erfahrungen erläutert und interpretiert. Die Struktur dieses Kapitels orientiert sich an den in \autoref{cha:konzept} vorgestellten Abschnitten.

\section{Devops-Prozesse}
\label{sec:auswertung:prozesse}
\forceindent Die qualitative Analyse der zu Beginn vorgestellten \glqq typischen\grqq{} DevOps-Prozesse gestaltet sich relativ unkompliziert und liefert eindeutige Ergebnisse, so dass die Auswahl von Prozessen für den Netzwerkbereich nicht schwer fällt. Diese Auswahl an Prozessen scheint prinzipiell unabhängig vom vorhandenen Umfeld zu sein -- lediglich \textit{Monitoring} könnte unter anderen Vorbedingungen noch hinzugefügt werden (falls noch kein Monitoring betrieben wird, was im Netzwerkbereich jedoch unüblich ist). 

In \autoref{tab:process-ev} wird der erwartete Nutzen der ausgewählten Prozesse, wie in \autoref{sec:analyse:prozesse} analysiert, aufgeführt:

\input{content/99_appendices/tables/process_expected_value.tex}

Aus dem Nutzen allein lässt sich jedoch noch keine Empfehlung bezüglich der Implementation ableiten, da hierfür auch der Aufwand selbiger in Betracht gezogen werden muss. 

\section{Tools}
\label{sec:auswertung:tools}
\forceindent Anders als die Auswahl der Prozesse ist die Auswahl passender Tools weit weniger eindeutig und kann keinen Anspruch auf Allgemeingültigkeit erheben. Für den Großteil der ausgewählten Prozesse kommen diverse Tools in Frage, die sich teilweise nur in Details unterscheiden. Aus diesem Grund muss sich die Auswahl der Tools immer stark am Umfeld orientieren, in welchem diese eingesetzt werden sollen.

In den meisten Fällen dürfte es sinnvoll sein, soweit wie möglich bereits bestehende Infrastruktur zu nutzen. Dies erleichtert einerseits den Einstieg, da die Hürden der Installation und Kofiguration wegfallen, andererseits  wird auch der tägliche Betrieb vereinfacht, da das Tool von mehr Teams bzw. Mitarbeitern bedient werden kann und keine separate Wartung etc. notwendig ist. Auch bereits vorhandene Fähigkeiten der beteiligten Mitarbeiter im Bezug auf die Tools (\zB Programmierkenntnisse) sind bei der Auswahl zu berücksichtigen.

Nicht zuletzt müssen auch die Präferenzen besonders derjenigen Mitarbeiter in Betracht gezogen werden, welche die Tools im täglichen Betrieb verwenden werden. Da die erfolgreiche Einführung neuer oder geänderter Prozesse letzten Endes immer von der Akzeptanz der Mitarbeiter abhängt, sollte der Übergang zu diesen neuen Tools so einfach wie möglich gestaltet werden.

\section{Implementation}
\label{sec:auswertung:implementation}
\forceindent Bei der Implementation einiger Tools zeigen sich Unterschiede zwischen erwartetem und tatsächlichem Aufwand, besonders die Umwandlung von Konfigurationsdateien in Templates ist hier hervorzuheben. Ab einer gewissen Größe des Netzwerkes dürfte dieser Schritt deutlich an Wert gewinnen, da sich die vorliegende Arbeit jedoch explizit auf kleine Netzwerke bezieht, sollte hier ein anderer Ansatz gewählt werden, mit Fokus auf die Erstellung eines Minimal-Templates. Einfacher als vermutet ist hingegen die Integration von Konfigurationsdatein in Git mittels Oxidized -- der Aufwand hierfür ist so gering, dass dieser Schritt in Anbetracht des gewonnenen Nutzens ausnahmslos in Netzwerken jeder Größe sinnvoll ist.

Das Skript zum automatischen Ausrollen von Konfigurationen ist dank der vorhandenen Frameworks und Module wenig komplex und mit Python-Vorkenntnissen ohne all zu großen Aufwand zu schreiben, lediglich die Syntax für die in GitLab integrierten CI/CD-Pipelines ist zu Beginn etwas gewöhnungsbedürftig. Eine saubere Trennung zwischen Daten (in diesem Fall die Konfigurationsdateien der Netzwerkgeräte) und Programmlogik macht hier das Entwickeln deutlich einfacher. 

Die folgende \autoref{tab:effort-value} erweitert \autoref{tab:process-ev} um den in \autoref{cha:implementation} festgestellten Implementationsaufwand:
\clearpage

\input{content/99_appendices/tables/effort_value.tex}

Aus den resultierenden Kosten-Nutzen-Verhältnissen lässt sich nun eine Reihenfolge ableiten, in welcher die Implementation der einzelnen Prozesse vorgenommen werden sollte:

\begin{enumerate}
    \item Übernahme der Konfigurationsdateien in ein Versionskontrollsystem (git)
    \item Erstellen eines Minimal-Templates für neue Geräte
    \item Continuous Integration für Konfigurationsdateien
    \item Tests für CI-Pipelines
    \item Semi-automatisches Deployment von Konfigurationsänderungen via CI/CD
    \item Optional: Umwandlung der gesamten Konfigurationsdateien in Templates
    \item Optional: Bei ausreichend hoher Testabdeckung kann das Deployment vollständig automatisiert werden
\end{enumerate}

Diese Liste enthält im Gegensatz zu den ursprünglich geplanten Schritten nicht mehr den Punkt \textit{Netzwerkvirtualisierung} bzw. das Ausführen von Tests gegen ein virtuelles Netzwerk. Grund hierfür sind die in \autoref{sec:implementation:virtual-tests} näher erläuterten Probleme, die sich aus den Unterschieden zwischen physischen und virtuellen Geräten ergeben.

\autoref{fig:recommended-flowchart} beschreibt die empfohlene Reihenfolge samt Abhängigkeiten der einzelnen Schritte, mit steigendem Aufwand von links nach rechts.

\input{content/99_appendices/figures/recommended_flowchart.tex}

Deutlich zu sehen ist dass die Entscheidung, ob und in welchem Umfang Konfigurationsdateien in Templates umgewandelt werden sollen, relativ unabhängig getroffen werden kann, die meisten anderen Schritte jedoch aufeinander aufbauen.

Es muss festgehalten werden, dass diese Empfehlung auf den Gegebenheiten einer konkreten Netzwerkinfrastruktur beruht, und sie daher für andere Umgebungen ggfs. modifiziert werden muss.
