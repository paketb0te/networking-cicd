\chapter{Implementation}
\label{cha:implementation}
\forceindent In diesem Kapitel soll die technische Implementation, wie in \autoref{sec:konzept:implementation} aufgeführt, dokumentiert werden. Die Gliederung dieses Kapitels erfolgt analog zu den dort genannten Schritten.

\section{Übernahme von Konfigurationsdateien in Git}
\forceindent Der erste Schritt, die Übernahme von Konfigurationsdateien in ein Versionskontrollsystem (Oxidized), war bei der System.de bereits getan. Um den Aufwand besser abschätzen zu können, wurde eine neue Instanz in einem virtuellen Lab eingerichtet.

Eine Basis-Installation ist relativ einfach aufzusetzen und ruft in bestimmten Zeitintervallen die Konfigurationsdateien aller definierten Geräte ab. Gibt es Änderungen zum letzen bekannten Zustand, so werden diese in einem commit in einem lokalen Git-Repository erfasst und, falls entsprechend konfiguriert, zu einem zentralen Git-Server gepusht.

Oxidized unterstützt unterschiedliche Quellen für sein Inventar abzufragender Geräte:

\begin{itemize}
    \item CSV
    \item SQL (SQLite und MySQL)
    \item HTTP
\end{itemize}

So können verschiedene Backends als Datenbank herangezogen und verknüpft werden, in kleinen Netzwerken ist die Verwaltung mittels einer CSV-Datei vermutlich ausreichend.

Das Aufsetzen einer solchen simplen Installation dauerte für den Autor als relativ unerfahrenen Admin etwa anderthalb Stunden -- inklusive Lesen der Doku, Anlegen der Virtuellen Maschine und Troubleshooting des systemd-scripts. Es ist also tatsächlich ein sehr überschaubarer Aufwand, der jedoch sofort einen erkennbaren Mehrwert bietet.

Oxidized bietet außerdem eine RESTful API, über welche \zB gezielt die Konfiguration eines Geräts abgerufen werden kann -- so könnte ein Syslog-Server konfiguriert werden, immer einen entsprechenden Aufruf an die API zu schicken, wenn die Konfiguration eines Gerätes geändert wurde (Auslöser wäre ein Monitoring der Syslog-Nachrichten auf entsprechende Strings).

\section{Umwandeln von Konfigurationen in Templates}
\forceindent Die Erstellung eines vollständigen Templates kann, abhängig von der Komplexität der Konfiguration, relativ zeitintensiv sein. Je nach Umgebung ist dabei der gewonnene Nutzen nicht direkt ersichtlich, besonders wenn nur Geräte eines einzelnen Herstellers verwendet werden (effektiv wird dann nur die Konfiguration in ein anderes Format übertragen).

Bei der Implementation stellte sich heraus, dass die Konfiguration der bei der System.de in Betrieb befindlichen Netzwerkgeräte aufgrund einer starken Segmentierung des Netzwerkes komplexer ist, als im Vorfeld der Arbeit angenommen. Der Aufwand für die Erstellung eines vollständigen Templates ist daher auch entsprechend höher -- einerseits wird das Template selbst komplexer und muss mehr Optionen für jeden Eintrag bieten, was zu geschachtelten if/else-Konstrukten und Loops führt, andererseits muss eine große Menge an Daten in ein anderes Format (hier YAML) umgeschrieben werden. Besonders der zweite Teil stellte sich als sehr zeitintensiv heraus.

Die Umwandlung ca. der Hälfte einer Konfigurationsdatei (etwa 650 von 1450 Zeilen) in Template und dazugehörige YAML-Datei dauerte etwa zehn Stunden. Da sich das manuelle Umwandeln derart zeitintensiv gestaltete, wurde dieser Ansatz umpriorisiert für eine etwaige spätere Implementation. Der Zeitaufwand steht mit geschätzten 12-14 Stunden pro Gerät im vorliegenden Fall in keinem sinvollen Verhältnis zum erwarteten Nutzen -- besonders im Anbetracht der Tatsache, dass die Netzwerkhardware der System.de aktuell von einem einzigen Hersteller stammt und damit der größte Vorteil der Umwandlung in Templates, die durch die Abstraktion gewonnene Unabhängigkeit von spezifischer Konfigurations-Syntax, aktuell kaum ins Gewicht fällt.

Als Alternative zu einer vollständigen Umwandlung in ein Template samt dazugehöriger Datenstruktur bietet es sich ggfs. an, ein \textit{Minimal-Template} für neue Geräte zu schreiben, welches lediglich die zur Inbetriebnahme erforderlichen Informationen enthält. Die detailiertere Konfiguration kann anschließend über die in Oxidized vorhandenen Konfigurationsdateien bearbeitet und auf das neue Gerät übertragen werden.

Die Variablen eines solchen Minimal-Templates könnten \zB folgende sein:

\begin{itemize}
    \item Hostname
    \item Benutzeraccounts
    \item VLANs
    \item VRFs
    \item Loopback Interfaces
\end{itemize}

Beispielimplementation eines Templates für Hostname, VRFs und Loopback Interfaces in Jinja2:

\lstinputlisting{content/99_appendices/code/minimal_template_hostname.j2}

Die Konfiguration des Hostnamens ist trivial, es wird lediglich eine Variable eingefügt. Die doppelten geschweiften Klammern dienen als Escape-Symbole, innerhalb derer auf Variablen zugegriffen werden kann.

\lstinputlisting{content/99_appendices/code/minimal_template_vrfs.j2}

Die Konfiguration der VRFs wird durch eine Schleife über alle in der Datenquelle definierten VRFs realisiert. Die geschweiften Klammern in Verbindung mit einem Prozentzeichen sind wiederum Escape-Symbole, welche jedoch die Definiton rudimentärer Logik wie Schleifen und if/then/else-Statements erlauben.

\lstinputlisting{content/99_appendices/code/minimal_template_loopbacks.j2}

Die Konfiguration der Loopback-Interfaces wird durch eine Schleife über alle in der Datenquelle definierten Loopback-Interfaces und darin geschachtelte if/then/else-Statements realisiert.

Datenquelle für dieses Template könnte eine YAML-Datei wie die folgende sein:

\lstinputlisting{content/99_appendices/code/host_vars.yml}

Wichtig ist, dass YAML die Möglichkeit bietet, verschachtelte Key-Value Paare darzustellen, so dass innerhalb einer Schleife auf die Sub-Keys eines Eintrages zugegriffen werden kann. So greift beispielsweise \lstinline{interface.ipv4_addr} in der Schleife zur Konfiguration der Loopback-Interfaces auf das Attribut \lstinline{ipv4_addr} des aktuell von der Schleife betrachteten Interfaces zu.


\section{Aufsetzen eines CI/CD-Tools}
\forceindent  Um die Machbarkeit zu demonstrieren und den Aufwand für die Einrichtung abzuschätzen, wurde zunächst ein kleines Lab angelegt, bestehend aus drei virtuellen Maschinen:  EVE-NG für die Virtualisierung der Netzwerkhardware, GitLab als Git- und CI/CD-Plattform, GitLab Runner (wird von GitLab für die Ausführung der CI/CD-Pipelines benötigt).

In GitLab wurden zwei separate Repositories (in GitLab \textit{Projekte} genannt) angelegt, eines für die Konfigurationsdateien und eines für die Logik, welche Tests ausführt und die Konfigurationen auf die Netzwerkgeräte überträgt. Diese Trennung ermöglicht die Weiterentwicklung des Tools, ohne bei jedem Commit die Pipeline zum Ausrollen der Konfiguration auszulösen.

\subsection{Konfigurations-Repository}
\forceindent Dieses Repository enthält die Konfigurationsdateien der zu verwaltenden Netzwerkgeräte. Es ist in zwei \textit{Branches} aufgeteilt, den Branch \lstinline{master} und einen Branch \lstinline{dev}. Änderungen am Master-Branch können nicht per git push erfolgen, sondern ausschließlich über Merge-Requests (der Branch wurde in GitLab als \textit{protected} markiert). Änderungen müssen also immer in \lstinline{dev} erfolgen und anschließend manuell in \lstinline{master} gemerged werden. So wird ein versehentliches Ändern der Gerätekonfigurationen verhindert.

Das Repository hat folgende Struktur:

\begin{samepage}
\begin{verbatim}
configuration_files
    router1.example.com
    router2.example.com
    ...
.gitlab-ci.yml
groups.yml
hosts.yml
\end{verbatim}
\end{samepage}

Im Verzeichnis \lstinline{configuration_files} liegen die Konfigurationsdateien für alle Netzwerkgeräte, wobei der FQDN des jeweiligen Gerätes als Dateiname verwendet werden muss, um eine Zuordnung zu ermöglichen. Die Dateien \lstinline{groups.yml} und \lstinline{hosts.yml} enthalten Informationen über die verwalteten Geräte (Hostname, Adresse, Betriebssystem, Zugangsdaten), welche für das Ausrollen der Konfigurationsdateien benötigt werden (siehe \autoref{sec:implementation:roll_out}).

Die von GitLab auszuführende Pipeline wird durch die Datei \lstinline{.gitlab-ci.yml} definiert.
Zu Beginn werden der zu verwendende Docker-Container, die unterschiedlichen Stufen der Pipeline und die Regeln, wann die Pipeline ausgeführt werden soll (wenn es Änderungen an Dateien im Verzeichnis \lstinline{configuration_files} gab), festgelegt:

\begin{samepage}
    \lstinputlisting[firstline=1, lastline=13]{../code/configs/.gitlab-ci.yml}
\end{samepage}

Anschließend werden per \lstinline{default before_script} Befehle definiert, die vor der Ausführung jedes Jobs ausgeführt werden -- das Rollout-Repository wird geklont und für die weitere Verwendung initialisiert:

\begin{samepage}
    \lstinputlisting[firstline=15, lastline=20]{../code/configs/.gitlab-ci.yml}
\end{samepage}

Es folgen die Definitionen der beiden Jobs für die Test- und Rollout-Stufe. Die Zeilen 12 - 15 stellen sicher, dass die Rollout-Stufe nur ausgeführt wird, wenn der auslösende Commit Teil eines Merge-Requests ist, welcher \lstinline{master} als Ziel hat.

\begin{samepage}
    \lstinputlisting[firstline=22, lastline=36]{../code/configs/.gitlab-ci.yml}
\end{samepage}

\subsection{Rollout-Repository}
\label{sec:implementation:roll_out}
\forceindent In diesem Repository liegt die Logik zum Verteilen der Konfigurationsdateien. Das Repository hat folgende Struktur:

\begin{samepage}
    \begin{verbatim}
    tests
        test_config_existence.py
        ...
    makefile
    nornir_config.yml
    requirements.txt
    roll_out_config.py
    \end{verbatim}
    \end{samepage}

Das Verzeichnis \lstinline{tests} beinhaltet die Tests, welche innerhalb der durch Konfigurationsänderungen ausgelösten Pipelines ausgeführt werden. Als Proof of Concept wurde zunächst ein trivialer Test geschrieben, der überprüft, ob für jeden Eintrag in der Datei \lstinline{hosts.yml} eine dazugehörige Konfigurationsdatei im Verzeichnis \lstinline{configuration_files} existiert, und umgekehrt. Leider existieren zum Zeitpunkt der Anfertigung dieser Arbeit keine sogenannten \textit{Linter} für Cisco-Konfigurationsdateien, weshalb die ursprünglich angedachte Syntax-Überprüfung nicht so einfach zu implementieren ist, wie angenommen. Prinzipbedingt können jedoch bei der Verwendung von Nornir als Automationsframework beliebige Tests definiert werden, solange sie in Python geschrieben werden können. Denkbar wären \zB auch die Verifikation des Gerätezustandes nach einer Änderung: Führt die Änderung am Routingprotokoll tatsächlich zum gewünschten Ergebnis? Und falls nein: Wie geht man damit um?

Das \lstinline{makefile} dient der Vereinfachung der Pipeline-Definition, da es erlaubt, in der Pipeline beispielsweise \lstinline{make test} auszuführen, die tatsächlich für diesen Job auszuführenden Befehle werden in das \lstinline{makefile} ausgelagert, um das Konfigurations-Repository so übersichtlich wie möglich zu halten.

Die Dateien \lstinline{nornir_config} und \lstinline{requirements.txt} werden für Konfiguration und Setup des eigentlichen Skripts benötigt.

Das Python-Skript \lstinline{roll_out_config.py} schließlich überträgt die Konfigurationsdateien auf die zugehörigen Geräte. Zu Beginn werden die benötigten Module geladen und Konstanten definiert. Die Funktion \lstinline{push_config()} wird von Nornir aufgerufen und ist für die Übertragun der Dateien verantwortlich. Dafür wird eine Funktion des NAPALM-Moduls verwendet, so dass lediglich die korrekten Parameter übergeben werden müssen, und anschließend das diff zwischen alter und neuer Konfiguration ausgegeben wird. Der Parameter \lstinline{dry_run} erlaubt es, die Konfigurationsdatei auf ein Gerät zu übertragen und lokal zu verifizieren, ohne eine Änderung an der laufenden Konfiguration vorzunehmen -- dies ist ein sehr wichtiger Schritt, um die syntaktische Korrektheit der Konfigurationsdatei sicherzustellen. Zusätzlich wird hier das diff aller \textit{potentiellen} Änderungen ausgegeben, falls die Konfigurationsdatei übertragen werden würde.

\begin{samepage}
    \lstinputlisting[language=Python, firstline=21, lastline=37]{../code/nornir_rollout/roll_out_config.py}
\end{samepage}

Ohne weitere Filterung würde sich diese Funktion mit allen im Inventar \lstinline{hosts.yml} befindlichen Geräten verbinden und die dort laufende Konfiguration mit der im Repository vorhandenen Konfigurationsdatei vergleichen. Bei den Geräten, bei denen sich die lokale Konfiguration von der im Repository unterscheidet wird anschließend die auf dem Gerät befindliche Konfiguration durch die Kofiguration im Repository ersetzt. Um diesen Overhead zu vermeiden, erlaubt es uns die Funktion \lstinline{get_changed_hosts()}, die in einem git commit (welcher durch die übergebene SHA-Prüfsumme spezifiziert wird) geänderten Dateien auszulesen und so eine Liste der Gerätenamen zu erzeugen, auf welche \lstinline{push_config()} angewendet werden soll:

\begin{samepage}
    \lstinputlisting[language=Python, firstline=40, lastline=60]{../code/nornir_rollout/roll_out_config.py}
\end{samepage}

Die Funktion \lstinline{parse_args()} ist eine Hilfsfunktion, um die beim Aufruf des Skriptes übergebenen optionalen Parameter \lstinline{--dry_run} und \lstinline{--commit_sha} einfacher verarbeiten zu können.

Die Funktion \lstinline{main()} steuert das gesamte Skript und ruft, je nach übergebenen Argumenten, die benötigten Funktionen mit den entsprechenden Parametern auf. Falls beim Aufruf ein Commit mittels SHA-Prüfsumme spezifiziert wurde, werden mittels der Nornir-eigenen Funktion \lstinline{filter()} nur die Hosts ausgewählt, deren Namen von der oben beschriebenen Funktion \lstinline{get_changed_hosts()} zurückgegeben werden.

\begin{samepage}
    \lstinputlisting[language=Python, firstline=83, lastline=106]{../code/nornir_rollout/roll_out_config.py}
\end{samepage}

Im letzten Schritt wird eine Zusammenfassung aller ausgeführten Tasks ausgegeben, wofür unterschiedliche Logging-Level gesetzt werden können.

\section{Aufsetzen einer Virtualisierungslösung}
\forceindent Zum Testen der Funktionen wurde zunächst eine Instanz von Eve-NG in einer lokalen VM installiert und in Betrieb genommen. 
Eine Topologie befindet sich in Eve-NG immer in einem \textit{Lab} und enthält \textit{Nodes} (virtuelle Geräte) und \textit{Networks}, welche diese verbinden (typischerweise entspricht dabei ein Network genau einem virtuellen Link zwischen zwei Nodes).

Nach erfolgreichem Transfer der zu verwendenden Images wurde ein Test-Lab angelegt und die grundlegenden Funktionalitäte der REST-API getestet. 

Beispielsweise lassen sich neue Nodes mit einem POST-Request erzeugen:

\lstinputlisting{content/99_appendices/code/eve_add_node.http}

Auf ähnliche Art werden Nodes verbunden, gestartet und angehalten -- der Zugriff und die Manipulation der virtuellen Umgebung per API gestaltet sich relativ unkompliziert.

Zu beachten sind die je nach verwendetem virtuellen Gerät relativ hohen Anforderungen an bentötigtem RAM (4GB pro Node für Cisco CSRv mit IOSv, 8GB für Cisco Nexus 9k mit XRv).

\section{Tests gegen virtuelles Netzwerk ausführen}
\label{sec:implementation:virtual-tests}
\forceindent Ursprünglich geplant war, an dieser Stelle die Brücke zwischen CI/CD und Virtualisierung zu schlagen, um innerhalb einer CI/CD-Pipeline automatisierte Tests in einem virtuellen Abbild des echten Netzwerkes auszuführen. 

Wie im letzten Absatz von \autoref{sec:analyse:virtualisation} bereits erwähnt, gibt es gewisse Unterschiede zwischen physischen Netzwerkgeräten und ihren virtuellen Klonen. Dies äußert sich zum einen in leicht abgewandelten Versionen der verwendeten Software (Die Software-Images \textit{basieren} zwar auf den Originalen, stimmen aber nicht exakt mit ihnen überein), die nicht immer den gleichen Funktionsumfang bieten. Zum anderen ergeben sich auch ganz praktische Probleme, wie \zB unterschiedliche Benennungen der Netzwerkinterfaces (\zB \lstinline{GigabitEthernet1/0/1} vs. \lstinline{GigabitEthernet1}).

Für diese Problem wäre vermutlich mit etwas Aufwand ein Workaround möglich: Sofern zumindest das Namensschema konstant bleibt, könnten die entsprechenden Stellen der Konfigurationsdateien dynamisch ersetzt oder durch ein Template erzeugt werden.

Ein weiteres, und letztendlich entscheidendes, Problem war, dass für die Geräte, die bei der System.de im Einsatz sind, zwar Software für die Verwendung in virtullen Umgebungen existiert, diese jedoch nur Layer 3 Operationen erlaubt (die Interfaces eines damit betriebenen virtuellen Gerätes können nicht als \textit{Switchport} konfiguriert werden). Da die Geräte bei der System.de als sogenannte L3-Switche verwendet werden (d.h. sie operieren sowohl auf Layer 2 als auch Layer 3 des OSI-Modells), lassen sich die Konfigurationsdateien der physischen Geräte gar nicht erst auf ihre virtuellen Klone übertragen. Aus diesem Grund konnte die geplante Implementation dieses Abschnittes nicht durchgeführt werden.