# Forschungsstand

## Infrastructure as Code

Als Infrastructure as Code (im folgenden IaC) wird der Ansatz bezeichnet, die Provisionierung und den Betrieb von Rechnern, Rechnerclustern und ganzen Rechenzentren vollständig durch maschinenlesbare Definitionsdateien zu realisieren.

Dadurch entfallen beispielsweise physische Hardwarekonfigurationen und interaktive Konfigurationstools.

Die Definitionsdateien werden oft in Versionskontrollsysteme eingebunden, wodurch gleichzeitig eine lückenlose Dokumentation der Infrastruktur-Konfiguration im Zeitverlauf möglich wird.

IaC erlebte einen initialen Anstieg in seiner Verbreitung im Jahr 2006 mit der Einführung von AWS (Amazon Web Services). Durch AWS hatten innerhalb kurzer Zeit auch kleine Firmen Zugriff auf IT-Infrastruktur in Größenordnungen, die bisher großen Unternehmen vorbehalten waren - mit allen Herausforderungen, die damit einhergehen. IaC wurde aus der Notwendigkeit heraus geboren, die Komplexität solcher Infrastruktur handhabbar zu machen.

### from nick russos course

- state declaration
- abstraction
- version control

## Software Defined Networking

## DevOps

### Begriffsklärung

Der Begriff "DevOps" ist in der wissenschaftlichen Literatur nicht einheitlich definiert \cite Erich et al, Jabbari et al: There is no consensus of what concepts DevOps covers, nor how DevOps is defined. (seite 4)

Aus diesem Grund wird zunächst eine Definition des Begriffes vorgenommen, wie er im weiteren Verlauf dieser Arbeit zu verstehen ist. Hierbei wird bewusst der Fokus auf die für den Netzwerkbetrieb relevanteren Bereiche gelegt.

DevOps ist ein Kunstwort aus den Begriffen "Development" ((Software-) Entwicklung) und "Operations" (IT-Betrieb) und beschreibt einen Ansatz,
welcher eine engere Zusammenarbeit zwischen den beiden (klassischerweise in unterschiedlichen Abteilungen angesiedelten) Teams ermöglichen und fördern soll. \cite{Schapiro.2018}

Dieser Ansatz umfasst nach den gängigen Definitionen drei Bereiche (oft bezeichnet als "DevOps Trinity"): Menschen, Prozesse \& Tools \cite{Farcic.2018}

Dies bedeutet, das DevOps nicht eine Ansammmlung bestimmter Prozesse und Tools ist, sondern viel mehr als eine Arbeitsphilosophie zu verstehen ist, welche den verschiedenen Stakeholdern eines Softwareprojektes durch gegenseitiges Aufgabenverständnis und gute Zusammenarbeit ermöglicht, ein besseres Produkt abzuliefern (wobei "Produkt" in diesem Zusammenhang nicht reiner Code ist, sondern das beim Endnutzer sichtbare Ergebnis, d.h. das Gesamtpaket aus qualitativ hochwertigem Code, hoher Verfügbarkeit bzw. Stabilität und kontinuierlicher Verbesserung).

Die konkreten Prozesse und Tools ergeben sich aus den Anforderungen, welche die Entwickler und Betreiber zur Erfüllung dieses Zieles definieren.

Trotz dieser grundsätzlich freien Wahl der Prozesse und Tools gibt es einige, die sich in vielen Umgebungen bewährt haben und daher oft als Teil von bzw. synonym zu DevOps betrachtet werden:

- agile Entwicklungsmethoden
- schnelle Iterationen
- hoher Automationsgrad
- Continuous Testing / Integration / Delivery / Deployment
- Continuous Integration
- Continuous Delivery
- Continuous Deployment

Diese Prozesse und Tools werden im weiteren auf ihre Relevanz und Anwendbarkeit für den Betrieb kleiner Rechnernetze untersucht.****

## NetDevOps

Das Thema
---
