# Einleitung

Netzwerk-Teams befinden sich heutzutage noch oft in einem sogenannten "Silo", d.h. sie sind im Hinblick auf Ihre Arbeitsabläufe,
Prozesse und zur Verfügung stehenden Informationen mehr oder weniger stark von anderen Teams innerhalb der IT (Entwicklung, Systemadministration, Security, ...) getrennt.

Dies führt nicht selten zu einigen der folgenden Probleme \cite[59]{Armstrong}:

- Reaktive Arbeitsweise
- Langsame Kollaboration über Ticketsysteme
- Manuelle Prozesse
- Keine Tests vor Produktivsetzung von Änderungen
- Fehler in manuellen Prozessen, die zu Ausfällen des Netzwerks führen
- permanent müssen "Brände" gelöscht werden (Es müssen andauernd Probleme gelöst werden, so dass keine Zeit für die gründliche Ursachenbehebung bleibt)
- Fehlende Automatisierung der täglichen Prozesse

Ähnliche Probleme kannte man bei Infrastrukturteams bzw. in der Systemadministration, wo sich seit einigen Jahren das Konzept von DevOps stark verbreitet hat \cite{}.

DevOps löst einige dieser Probleme durch engere Zusammenarbeit zwischen den beteiligten Teams und einen möglichst hohen Grad an Automatisierung.

Auch im Bereich der Netzwerkadministration findet dieser Ansatz unter dem Namen NetDevOps langsam Anklang, allerdings bisher primär in Rechenzentren und großen bis sehr großen Netzwerken (250 und mehr Netzwerkgeräte) \cite{dgarros}.

In der folgenden Arbeit soll daher untersucht werden, welche Prozesse und Tools auch für kleinere Netzwerke einen Mehrwert bringen, und ob der Implementierungsaufwand in einem angemessenen Verhältnis zu diesem steht.
Weiterhin soll evaluiert werden, inwiefern sich virtualisierte Netzwerke (als mehr oder weniger exakter „Klon“ des produktiven Netzwerkes) für automatisierte Tests innerhalb einer CI/CD Pipeline eignen.
