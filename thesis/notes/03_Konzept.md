# Konzept

Da in dem Unternehmen, für welches die Implementation der später auszuwählenden Prozesse und Tools durchgeführt werden soll, bisher keine separaten Organisationseinheiten für Entwicklung und Betrieb existieren, wird der Fokus der Arbeit primär auf der Analyse von Prozessen und Tools liegen, und der oft beschriebene kulturelle Aspekt nicht intensiver bearbeitet.
