# Implementation

## Plan

1. Übernahme der Konfigurationsdateien in ein Versionskontrollsystem (git)
1. Umwandeln von Konfigurationen in Templates
1. Aufsetzen eines CI/CD Tools
1. Einfache Tests als Proof of Concept für CI/CD-Pipelines schreiben
1. (semi-) automatisches Deployment von Konfigurationsänderungen
1. Virtualisierungslösung aufsetzen
1. Tests gegen virtuelles Netzwerk ausführen

## Übernahme der Konfigurationsdateien in ein Versionskontrollsystem (git)

- ist das schon gegeben durch oxidized?
- eigenes "demo-oxidized" bauen? wäre vielleicht gut, kann chris evtl schnell zeigen
- was wäre die Alternative?

### Oxidized aufsetzen

Offizielle Anleitung: <https://github.com/ytti/oxidized#installation>

Diverse Möglichkeiten, oxidiuzed auszuführen:

- Direkt in der Distro der Wahl &rarr; ruby per Paketmanager installieren, oxidized als gem installieren.
- Aus git-repo selbst kompilieren
- Als Docker-Container

Eine Basis-Installation ist relativ einfach aufzusetzen und ruft in konfigurierbaren Zeitintervallen die Konfigurationsdateien alle definierten Geräte ab. Gibt es Änderungen zum letzen bekannten Zustand, so werden diese in einem git commit in einem lokalen repository erfasst und, falls entsprechend konfiguriert, zu einem zentralen Git-Server gepusht.

Oxidized unterstützt unterschiedliche Quellen für seine Liste abzufragender Geräte:

- csv
- SQLite
- MySQL
- HTTP

So können verschiedene Backends als Datenbank herangezogen und verknüpft werden.

In kleinen Netzwerken ist das Anlegen einer Textdatei vermutlich ausreichend.

Das Aufsetzen einer solchen simplen Installation dauerte für mich als relativ unerfahrenen Admin ca anderthalb Stunden - inklusive Lesen der Doku, anlegen der VM und troubleshooting des systemd-scripts. Es ist also tatsächlich ein sehr überschaubarer Aufwand, der jedoch sofort einen erkennbaren Mehrwert bietet.

Oxidized bietet ausserdem eine RESTful API, über welche zB gezielt die Konfiguration eines Geräts abgerufen werden kann - so könnte ein ein syslog-server konfiguriert werden, auf bestimmte Nachrichtentypen zu achten und immer einen entsprechenden Aufruf an die API zu schicken, wenn die Konfiguration eines Gerätes geändert wurde.

## Umwandeln von Konfigurationen in Templates

- [x] Entscheidung für Templating-Sprache Jinja weil bekannt + kompatible mit bestehenden tools
- [x] reale config ziehen
- [ ] in templates umwandeln
- [x] config aus templates erzeugen &rarr; diff == 0 (?)

Die Erstellung eine vollständigen Templates kann, abhängig von der Komplexität der Konfiguration, relativ zeitintensiv sein. Je nach Umgebung ist dabei der gewonnene Nutzen nicht direkt ersichtlich, besonders wenn nur Geräte eines einzelnen Herstellers verwendet werden (effektiv wird dann nur die Konfiguration in ein anderes Format übertragen).

Es bietet sich daher ggfs. an, ein sinnvolles *Minimal-Template* für neue Geräte zu schreiben, welches lediglich die zur Inbetriebnahme erforderlichen Informationen enthält. Die Interface-Konfigurationen etc. können anschließend über die in Oxidized vorhandenen Konfigurationsdateien bearbeitet werden.

### Was ist minimal

- nostname
- interface defaults
- user
- routing?
- ACLs?
- CoS, Class-Maps etc?

## Aufsetzen eines CI/CD Tools

- [x] gitlab aufsetzen
- [x] projekt anlegen
- [x] vernünftiges branching (master, test, dev?)
- [x] (dummy-) pipeline definieren (mit möglichkeit, per flag zu entscheiden ob sie erfolgreich ist oder nicht)
- [x] push immer nur in dev
- [x] pipelines müssen erfolgreich sein vor merge in einen anderen branch
- [ ] nornir return values: können non-zero values zurückgegeben werden, wenn das ausrollen auf einen host fehlsschlägt? macht das sinn?

Um die Machbarkeit zu demonstrieren und den Aufwand für die Einrichtung abzuschätzen wurde zunächst ein kleines Lab angelegt,  bestehend aus drei viruellen Maschinen:  EVE-NG für die Virtualisierung der Netzwerkhardware, Gitlab als Git- und CI/CD-Plattform, Gitlab Runner (wird von Gitlab für die Ausführung der CI/CD-Pipelines benötigt).

In Gitlab wurden zwei separate Repositories / Projekte angelegt, eines für die Konfigurationsdateien und eines für die Logik, welche diese Dateien auf die Netzwerkgeräte überträgt.

### Konfigurations-Repository

Dieses Repository ist in zwei sogenannte *Branches* geteilt, den Master-Branch und einen Development-Branch. Änderungen am Master-Branch können nicht per `git push` erfolgen, sondern ausschließlich über Merge-Requests (der Master-Branch ist "protected"). So wird ein versehentliches Ändern der Gerätekonfigurationen verhindert. Das Repository hat folgende Struktur:

```bash
.
├── configs
│   ├── Router1.ios
│   └── Router2.ios
├── .gitlab-ci.yml
├── groups.yaml
└── hosts.yaml
```

Im Ordner `configs` liegen die Konfigurationsdateien für alle Netzwerkgeräte.

Die Dateien `groups.yaml` und `hosts.yaml` enthalten Informationen über die verwalteten Geräte (Hostnamwn / Adresse, Betriebssystem, Zugangsdaten). Diese werden für das Ausrollen der Konfigurationsdateien benötigt.

Die von Gitlab auszuführende Pipeline wird durch die Datei `.gitlab-ci.yml` definiert:

```yaml
image:
  name: python:3.8.2-buster

stages:
  - test
  - rollout

test:
  stage: test
  script:
    - ping -c 1 router1.manu.lab
    - ping -c 1 router2.manu.lab

rollout:
  stage: rollout
  script:
    - git clone http://gitlab.manu.lab/root/nornir_cicd.git
    - pip install -r nornir_cicd/requirements.txt
    - cp nornir_cicd/roll_out_config.py .
    - python roll_out_config.py
  rules:
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME == "dev" && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "master"'
```

Die Pipeline führt bei einer Änderung bestimmte Tests aus, welche die Konfigurationsdateien validieren (definiert im Block `test`). Der zweite Block `rollout` wird nur ausgeführt, wenn die Änderung ein Merge von Dev in Maser ist. In diesem Fall wird das andere Repository, welches die Logik zum Ausrollen der Konfigurationsdateien enthält, geklont, die notwendigen Pakete installiert und anschließend das entsprechende Skript gestartet, welches die geänderten Konfigurationsdateien an die entsprechenden Geräte verteilt.

### Rollout-Repository

Dieses Repository kann für die Weiterentwicklung auch in unterschiedliche Branches aufgeteilt werden, für den normalen Betrieb ist die jedoch nicht notwendig. Selbstverständlich sollte auch in diesem Repository eine Pipeline zur Validierung von Änderungen am Code implementiert werden, dies ist jedoch normale Praxis der Softwareentwicklung und soll daher an dieser Stelle nicht weiter ausgeführt werden.

Für den Betrieb sind die Dateien `requirements.txt` und `roll_out_config.py` relevant. Erstere beinhaltet eine Liste aller benötigten Pakete, welche per `pip install` installiert werden, letztere ist das Skript, welches die Konfigurationsdateien verteilt.

```python
import logging
import os
from nornir import InitNornir
from nornir.plugins.tasks.networking import napalm_configure
from nornir.plugins.functions.text import print_result

CONFIG_DIR = "configs"


def push_config(task) -> None:
    """
    pushes config
    """
    # push config to device
    task2_result = task.run(
        task=napalm_configure,
        dry_run=False,
        filename=os.path.abspath(f"{CONFIG_DIR}/{task.host.name}.ios"),
        replace=True,
    )
    # if there was a diff, print it to screen
    if task2_result[0].diff:
        print(f"{task.host.name}: diff below\n{task2_result[0].diff}")
    else:
        print(f"{task.host.name}: no diff; config up to date")


def main():
    """
    Execution begins here.
    """

    # Initialize nornir, nothing new yet.
    nornir = InitNornir()

    # We can run arbitrary Python code, so let's just print the
    # hostnames loaded through the inventory, which are dict keys.
    print("Nornir initialized with inventory hosts:")
    for host in nornir.inventory.hosts.keys():
        print(host)

    # Invoke the grouped task.
    result = nornir.run(task=push_config)

    # Use Nornir-supplied function to pretty-print the result
    # to see a recap of all actions taken. Standard Python logging
    # levels are supported to set output verbosity.
    print_result(result, severity_level=logging.WARNING)


if __name__ == "__main__":
    main()
```

## Einfache Tests als Proof of Concept für CI/CD-Pipelines schreiben

- linter
  - gibt es linter?
  - falls nicht: config auf virtuelles device einspielen und anschließend config vergleichen?
- für templates: rendern und anschließend linten?

## (semi-) automatisches Deployment von Konfigurationsänderungen

- [x] nach merge in master &rarr; deployment via ansible / nornir
- [x] Entscheidung: Ansible oder Nornir?
- [x] **Trennung von Rollout-Code und config! &rarr; separate repos**

## Virtualisierungslösung aufsetzen

- Eve-NG hat chris bereits aufgesetzt, aber ich kann ja schreiben wie ich mein Lab aufgesetzt hab
- installation in vmware player
- images überspielen
- API tests

## Tests gegen virtuelles Netzwerk ausführen

- virtuelles Metz bauen

### Mögliche Tests

- was wären sinnvolle tests?
  - sachen die sich nicht auf die reine config beziehen!
  - routing table
  - ospf adjacencies
  - cdp neighbors
  - operational link state
  - ...

### API stuff

- Postman Collection bauen

#### Zwei Nodes verbinden

Neues Netzwerk via POST anlegen, gibt ID zurück:

```http
POST http://{{EVE_HOST}}/api/labs/{{LAB_PATH}}/networks

{
  "count":1,
  "name":"Net-CSRiface_2",
  "type":"bridge",
  "left":885,
  "top":862,
  "visibility":1,
  "postfix":0
}
```

Die gewünschten Interfaces der zu verbindenden Nodes auf das gerade erzeugte Netzwerk mappen (Hier: Interface mit ID 1 wird zu Netzwerk 5 hinzugefügt):

```http
PUT http://{{EVE_HOST}}/api/labs/{{LAB_PATH}}/nodes/NODE_ID/interfaces

{
  "1":5
}
```

Analog für anderes Ende des Links ausführen.

Anschließend das Netzwerk unsichtbar machen (rein kosmetisch für die Darstellung im Webinterface):

```http
PUT http://{{EVE_HOST}}/api/labs/{{LAB_PATH}}/networks/{{NETWORK_ID}}

{
  "visibility":0
}
```

#### Node hinzufügen

```http
POST http://{{EVE_HOST}}/api/labs/{{LAB_PATH}}/nodes

{
  "type":"qemu",
  "template":"vios",
  "config":"Unconfigured",
  "delay":0,
  "icon":"c_router_blue.png",
  "image":"vios-adventerprisek9-m,vmdk.SPA.157-3.M3",
  "name":"Router_1",
  "left":"35%",
  "top":"25%",
  "ram":"1024",
  "console":"telnet",
  "cpu":1,
  "ethernet":2
}
```
