# Auswertung

## Erwartungen

- Leslie Carr SRECon16: What is NetDevOps? Why?
  
1. Start Using Git
2. check in ALL configs into git
3. templatize config files
4. Team Separation
5. continuous Integration Systems
   1. Typos mit wordlist
   2. tests auf der konfig
6. virtuelle Kopie des Netzwerks
7. post mortems
   1. feel comfortable failing

## Ergebnisse: Was habe ich herausgefunden

1. Konfigurationen in Git übernehmen ist super einfach!
1. Templating ist (bei system.de) aufwendiger als gedacht
1. Konfigurationen Repo -> Device ist gut machbar, wenn man die Funktionsweise von gitlab-ci.yml verstanden hat
1. Eve per API ansteuern funktioniert gut
1. Automatisierte Tesrs gegen virtuelles Netzwerk leider nicht möglich, da Unterschiede zu groß
1. Tool-Auswahl kaum objektiv möglich, hängt stark von den Gegebenheiten der Umgebung ab.

- [ ] Kosten/Nutzen-Analyse führt zu einer Änderung der ursprüglich geplanten Reihenfolge
- [ ] Unterschiede zwischen echten und virtuellen Geräten größer als gedacht!
- [ ] Daher Tests gegen virtuelles Netzwerk für das vorliegende Umfeld nicht bzw. nicht mit vertratbarem Aufwand machbar.
- [ ] In anderen Umgebungen (RZ, Cumulus?) jedoch evtl. möglich (exakt gleiche images?)
- [ ] (technische) Implementation eines Git-Workflows unkomplizierter als gedacht, Adaption im Team muss sich noch zeigen

## Begrenzungen

- [ ] Fokus auf kleine Netzwerke, daher Fokus eher auf Kosten/Nutzen als auf genereller Machbarkeit
- [ ] Die für System.de evaluierte Reihenfolge ist unter Umständen nicht direkt auf andere Netze übertragbar, bietet aber einen Orientierungspunkt.

## Vorschläge für weitere Forschung

## Struktur

1. 