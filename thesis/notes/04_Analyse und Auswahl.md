# Analyse und Auswahl

## Analyse der Prozesse

### Monitoring

*Monitoring* bedeutet im weitesten Sinne die Überwachung verschiedener Systemparameter und ein laufender Abgleich zwischen Soll- und Ist-Zustand. Die Zugrundeliegende Frage ist: "Tut das System das, was es tun soll?" \cite{schlossnagle}

Typischerweise geht damit auch eine Benachrichtigungsfunktion einher, so dass bei Abweichungen reagiert werden kann.

Technisch werden dabei meistens Parameter wie CPU-Last, Speicherverbrauch, freier Festplattenplatz, Netzwerkstatistiken und ähnliches überwacht und in einem zentralen Monitoring-Tool zusammengeführt. Zusätzlich werden oft auch Ende-zu-Ende-Metriken wie \zb Ladezeiten einer Webseite aus Nutzerperspektive überwacht. Falls die Möglichkeit besteht diese Daten (ggfs. in gröberer Auflösung) für längere Zeiträume vorzuhalten ergeben sich auch Möglichkeiten für statistische Auswertungen, die für die Kapazitätsplanung hilfreich sein können.

Monitoring steht im DevOps-Kontext vor zwei besonderen Herausforderungen \cite{schlossnagle}:

1. Im Vergleich zum klassischen IT-Betrieb passieren Änderungen wesentlich häufiger und schneller. Dies führt dazu, dass sich das Verhalten des Systems unter Umständen kontinuierlich ändert und die *baseline*, also der gewüscnhte Soll-Zustand, für sinnvolles Monitoring entsprechend angepasst werden muss.
2. Der Einsatz von Microservice-Architekturen und die damit einhergehende Flexibilität führt dazu, dass sich die Struktur des System ggfs. häufiger ändert -- auch für diese Änderungen muss das Monitoring angepasst werden.

Wenn durch die Anwendung von DevOps-Prozessen auch im Netzwerkbetrieb eine höhere Flexibilität sowie schnellere und häufigere Änderungen erreicht werden, wird Punkt eins auch für das Netzwerkmonitoring relevant. Da der erwartete Nutzen dieser Prozesse (zumindest für kleine Netze) eher in verbesserter Nachvollziehbarkeit und höherer Stabilität als in schnellen und häufigen Änderungen liegt, wird die Anpassung des Monitorings hier nicht näher betrachtet. Denkbar sind weiterführende Untersuchungen in dieser Richtung besonders für Serviceprovider-Netzwerke, da hier regelmäßig Änderungen durchgeführt werden.
