"""
Author: Manuel Stausberg
Purpose: PoC to roll out configurations to networking devices
"""

import argparse
import logging
import os
from git import Repo
from nornir import InitNornir
from nornir.plugins.tasks.networking import napalm_configure
from nornir.plugins.functions.text import print_result
from nornir.core.filter import F

# remember that this repo will be cloned INTO the config-repo
# and therefore should always use relative paths
CONFIG_REPO_PATH = "../"
CONFIG_DIR = "configuration_files/"


def push_config(task, dry_run=False) -> None:
    """
    pushes config files to all hosts passed by the nornir object.
    Prints the diff in config if there is any.
    """
    # push config to device
    task_result = task.run(
        task=napalm_configure,
        dry_run=dry_run,
        filename=os.path.abspath(f"{CONFIG_REPO_PATH}{CONFIG_DIR}{task.host.hostname}"),
        replace=True,
    )
    # if there was a diff, print it to screen
    if task_result[0].diff:
        print(f"{task.host.name}: diff below\n\n{task_result[0].diff}")
    else:
        print(f"{task.host.name}: no diff; config up to date")


def get_changed_hosts(commit_sha: str) -> list():
    """
    given a git commit-SHA, returns a list of hostnames corresponding to
    configuration files that have been changes in the passed commit
    """
    # initialize the repo of the outer config repo
    repo = Repo(os.path.abspath(f"{CONFIG_REPO_PATH}.git"))
    git = repo.git
    # get the list of changed files for the given commit-SHA
    changed_filenames = str(
        git.diff_tree("--no-commit-id", "--name-only", "-r", commit_sha)
    )
    changed_list = changed_filenames.split()
    # drop all changed files that are not config files (in the config directory)
    # and extract the hostnames
    changed_hostnames = []
    for filename in changed_list:
        if filename.startswith(CONFIG_DIR):
            changed_hostnames.append(filename[len(CONFIG_DIR) :])

    return changed_hostnames


def parse_args() -> argparse.Namespace:
    """
    returns the parsed arguments as argparse.Namespace object.
    This can be acessed just like when using argparse directly.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dry_run",
        help="execute a dry run, not writing changes to devices",
        action="store_true",
    )
    parser.add_argument(
        "--commit_sha",
        type=str,
        help="if passed, execute only on hosts with changed configuration files",
    )

    return parser.parse_args()


def main(args: argparse.Namespace):
    """
    Execution begins here.
    """
    # Initialize nornir
    nornir = InitNornir(config_file="nornir_config.yml")
    print("Rolling out changes to:")
    # if a commit-SHA was passed, run the taks only against
    # hosts for which the config file has been changed
    if args.commit_sha:
        changed_hosts = get_changed_hosts(commit_sha=args.commit_sha)
        for host in changed_hosts:
            print(host)
        result = nornir.filter(F(hostname__any=changed_hosts)).run(
            task=push_config, dry_run=args.dry_run
        )
    # else roll out config files to all hosts in the inventory
    else:
        for host in nornir.inventory.hosts.keys():
            print(host)
        result = nornir.run(task=push_config, dry_run=args.dry_run)
    # pretty-print the result to see a recap of all actions taken.
    # Python logging levels can be used to set output verbosity.
    print_result(result, severity_level=logging.WARNING)


if __name__ == "__main__":
    parsed_args = parse_args()
    main(args=parsed_args)
