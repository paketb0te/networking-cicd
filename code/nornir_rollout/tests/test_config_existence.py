"""
Tests to verify config files
"""
import os

RELATIVE_PATH = "../"
CONFIG_DIR = RELATIVE_PATH + "configuration_files/"
HOSTS_FILE = RELATIVE_PATH + "hosts.yml"


def test_config_existence():
    """
    verifies that:
    - for each host listed in HOST_FILE
    - a corresponding config file exists in CONFIG_DIR.
    """

    # create a set of all hostnames present in the hosts.yml file
    host_names = set()

    with open(HOSTS_FILE, "r") as file:
        lines = file.readlines()

    for line in lines:
        if "hostname:" in line:
            name = line.split(":")[1].strip()
            host_names.add(name)

    # create a set of all config files in the configs/ directory
    config_names = set()

    for filename in os.listdir(CONFIG_DIR):
        config_names.add(filename)

    # get the differences between both sets and verify that they are empty sets
    hosts_without_config = host_names - config_names
    configs_without_host = config_names - host_names

    assert len(hosts_without_config) == 0, "There is a Host without Config!"
    assert len(configs_without_host) == 0, "There is a Config without Host!"
